
Git global setup

git config --global user.name "Arnab Das"
git config --global user.email "arnab.das682@gmail.com"

Create a new repository

git clone https://gitlab.com/Jmidar/Arnab_176285_B58_S3_git.git
cd Arnab_176285_B58_S3_git
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/Jmidar/Arnab_176285_B58_S3_git.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/Jmidar/Arnab_176285_B58_S3_git.git
git push -u origin --all
git push -u origin --tags
